// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import axios from 'axios'
import router from './router'


if (location.protocol != 'https:')
{
    location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
}else {

    var token = localStorage.getItem('token');

    if (token == null || token == undefined) {
        console.log('YOU ARE NOT LOGGED');
        router.replace('/login');
    } else {
        console.log('TOKEN : ' + token);
    }


    Vue.config.productionTip = false

    /*
     * RESPONSE INTERCEPTOR
     */
    axios.interceptors.response.use(function (response) {

            return response;

        }, function (error) {

            if (401 === error.response.status) {
                localStorage.removeItem('token');
                router.replace('/login');
            } else {
                return error;
            }

        }
    );

    /* eslint-disable no-new */
    var app = new Vue({
        el: '#app',
        router,
        template: '<App/>',
        components: {App}
    });
}
