import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/modules/meobox/Dashboard'
import Login from '@/modules/meobox/Login'
import Logout from '@/modules/meobox/Logout'
import Users from '@/modules/meobox/Users'
import Projects from '@/modules/meobox/Projects'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
        path: '/logout',
        name: 'Logout',
        component: Logout
    },
    {
      path: '/users',
      name: 'Users',
      component: Users
    },
    {
      path: '/projects',
      name: 'Projects',
      component: Projects
    },
  ]
})
