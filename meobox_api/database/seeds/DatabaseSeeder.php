<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Nina',
            'last_name' => 'Zbinden',
            'avatar' => 'http://www.2chefspassion.co.uk/wp-content/uploads/2015/08/testimonial-woman-5-star-review.png',
            'birthdate' => '1989-09-26',
            'email' => 'nina.zbinden@meomeo.ch',
            'password' => bcrypt('1658meopass'),
            'role' => 'acc_manager',
        ]);

        DB::table('users')->insert([
            'first_name' => 'Myriam',
            'last_name' => 'Brulhart',
            'avatar' => 'http://www.2chefspassion.co.uk/wp-content/uploads/2015/08/testimonial-woman-5-star-review.png',
            'birthdate' => '1989-09-26',
            'email' => 'myriam.brulhart@meomeo.ch',
            'password' => bcrypt('1658meopass'),
            'role' => 'acc_manager',
        ]);
    }
}
