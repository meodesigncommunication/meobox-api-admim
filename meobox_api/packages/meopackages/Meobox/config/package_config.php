<?php
/**
 * Created by PhpStorm.
 * User: kylemobilia
 * Date: 11.12.17
 * Time: 15:10
 */

return [

    'package_name' => 'MeoBox',

    'teamwork' => [
        'key'  => 'twp_1nVE2ry68YmSVwvWNLjP3yqtA2ue',
        'url'  => 'https://meo3.teamwork.com/'
    ],

    'user_table' => [
        'first_name' => 'Prénom',
        'last_name' => 'Nom',
        'fees' => 'Honoraires (CHF)',
        'email' => 'Email',
        'role' => 'Role'
    ],

    'default_fees' => 150

];