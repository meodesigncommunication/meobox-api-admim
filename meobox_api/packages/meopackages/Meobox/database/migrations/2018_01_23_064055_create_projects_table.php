<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('project_meo_id');
            $table->integer('contact_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('po_client');
            $table->float('budget');
            $table->float('suppliers');
            $table->timestamps();
            $table->softDeletes();
            $table->primary('id');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
