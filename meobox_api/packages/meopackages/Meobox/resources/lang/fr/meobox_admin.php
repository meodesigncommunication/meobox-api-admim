<?php
/**
 * Created by PhpStorm.
 * User: kylemobilia
 * Date: 09.01.18
 * Time: 14:16
 */

return [
    'email' => 'Adresse email',
    'password' => 'Mot de passe',
    'remember_me' => 'Se rappeler de moi',
    'forgot_password' => 'Mot de passe oublié ?',
    'connection' => 'Se connecter',
    'login_error' => 'Votre email ou mot de passe est incorrect',
];