<?php

namespace Meopackages\Meobox\Http\Controllers;

use App\Http\Requests;
use Meopackages\Meobox\Models\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class MeoboxAuthController extends Controller
{

    // User Login Get Token
    public function login(Request $request){

        $credentials = $request->only('email', 'password'); // grab credentials from the request
        try {
            if (!$token = JWTAuth::attempt($credentials)) { // attempt to verify the credentials and create a token for the user
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500); // something went wrong whilst attempting to encode the token
        }

        return response()->json(['token' => $token]);

    }

    // Refresh Token
    public function token(){

        $token = JWTAuth::getToken();

        if(!$token){

            throw new BadRequestHtttpException('Token not provided');

        }
        try{

            $token = JWTAuth::refresh($token);

        }catch(TokenInvalidException $e){

            throw new AccessDeniedHttpException('The token is invalid');

        }

        return $this->response->withArray(['token'=>$token]);
    }

    // Get Auth User Data
    public function getAuthUser(Request $request){

        try {

            $user = JWTAuth::toUser($request->token);
            return response()->json(['result' => $user]);

        }catch(JWTException $e) {

            return response()->json(['token' => false], 401);

        }
    }

    // Logout User
    public function logout(Request $request) {

        $token = $request->token;

        return response()->json(['token' => $token]);

        /*JWTAuth::invalidate($token);

        return response()->json(['success'], 200);*/

    }
}