<?php

namespace Meopackages\Meobox\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Meopackages\Meobox\Models\Contacts as Contacts;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class MeoboxContactsController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getContacts(Request $request)
    {
        $contacts = Contacts::all();
        return response()->json(['status' => true, 'contacts' => $contacts], 200);
    }
}