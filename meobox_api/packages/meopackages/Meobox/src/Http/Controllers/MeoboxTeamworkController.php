<?php

namespace Meopackages\Meobox\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Meopackages\Meobox\Http\Helpers\TeamworkHelper;
use Meopackages\Meobox\Models\Projects as Projects;
use Meopackages\Meobox\Models\Users as Users;
use GuzzleHttp\Client as Guzzle;
use Rossedman\Teamwork\Client;
use Rossedman\Teamwork\Factory as Teamwork;

class MeoboxTeamworkController extends BaseController
{

    /*
     * TEAMWORK USERS
     */
    public function getTeamworkPeoples() {

        $users = Users::all();

        $teamworkHelper = new TeamworkHelper();

        $peoples = json_decode(json_encode($teamworkHelper->getPeoples()), true);

        foreach($peoples as $people) {

            $exist = false;

            foreach($users as $user) {

                if($people['email-address'] == $user->email) {
                    $exist = true;
                }

            }

            if(!$exist) {

                $user = new User();

                $user->first_name = $people['first-name'];
                $user->last_name = $people['last-name'];
                $user->avatar = 'http://www.semfe.ntua.gr/media/k2/items/cache/2ff2ba0051687eef5ca0459cf942940c_L.jpg';
                $user->birthdate = '1980-01-01';
                $user->email = $people['email-address'];
                $user->password = bcrypt('1658meopass');
                $user->role = 'user';
                $user->fees = config('meobox_package_config.default_fees');

                $user->save();

            }

        }

        return response()->json(['status' => true], 200);
    }


    /*
     *  TEAMWORK PROJECT
     */

    public function getProjects() {

        $teamworkHelper = new TeamworkHelper();

        $projects = $teamworkHelper->getProjects();

        return response()->json(['status' => true, 'projects' => $projects], 200);
    }

    public function searchProject(Request $request) {

        $teamworkHelper = new TeamworkHelper();

        $projects = $teamworkHelper->getProjectsBySearchTerm($request->searchTerm);

        return response()->json(['status' => true, 'projects' => $projects], 200);
    }

    public function saveProject(Request $request) {

        $getProjectCount = Projects::where('id' ,$request->project['id'])->count();

        if(!$getProjectCount) {

            $project = new Projects();
            $project->id = $request->project['id'];

        }else{

            $project = Projects::find($request->project['id']);

        }

        $project->project_meo_id = $request->project['project_meo_id'];
        $project->contact_id = $request->project['contact'];
        $project->user_id = $request->project['user'];
        $project->po_client = $request->project['po_client'];
        $project->budget = $request->project['budget'];
        $project->suppliers = $request->project['suppliers'];

        if( $project->save() ){

            $teamworkHelper = new TeamworkHelper();
            $projects = $teamworkHelper->getProjectsBySearchTerm($request->project['name']);

            return response()->json(['status' => 1, 'project_id' => $request->edit_id, 'projects' => $projects], 200);

        }else{

            return response()->json(['status' => 0], 200);

        }
    }
}