<?php

namespace Meopackages\Meobox\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Meopackages\Meobox\Models\Users as Users;
use Illuminate\Routing\Controller as BaseController;

class MeoboxUsersController extends BaseController
{

    public function register(Request $request){

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'avatar' => 'required|max:255',
            'birthdate' => 'required|date',
            'email' => 'required|unique:users|max:255',
            'password' => 'required|max:255',
            'role' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => 'invalid_data'], 401);
        }

        $data = [
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'avatar' => $request->get('avatar'),
            'birthdate' => $request->get('birthdate'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password'))
        ];

        $user = Users::create($data);

        return response()->json(['status'=>true,'message'=>'User created successfully','data'=>$user], 200);

    }

    public function getUsers(Request $request){

        $header_table = config('meobox_package_config.user_table');
        $users = Users::all();
        return response()->json(['status'=>true, 'users' => $users, 'header_table' => $header_table], 200);

    }

    public function getUsersAccManager(Request $request){

        $header_table = config('meobox_package_config.user_table');
        $users = Users::where('role',  'acc_manager')->get();
        return response()->json(['status'=>true, 'users' => $users, 'header_table' => $header_table], 200);

    }

    public function getUser(Request $request){

        $id = $request->id;
        $header_table = config('meobox_package_config.user_table');
        $user = Users::findOrFail($id);
        return response()->json(['status'=>true,'user'=>$user, 'header_table' => $header_table], 200);

    }
}