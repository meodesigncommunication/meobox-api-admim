<?php

namespace Meopackages\Meobox\Http\Helpers;

// USE VENDOR
use GuzzleHttp\Client as Guzzle;
use Rossedman\Teamwork\Client;
use Rossedman\Teamwork\Factory as Teamwork;

// USE MEOPACKAGES
use Meopackages\Meobox\Models\Users;
use Meopackages\Meobox\Models\Projects;
use Meopackages\Meobox\Models\TaskLists;

class TeamworkHelper
{

    private $secretKey = '';
    private $url = '';

    public function __construct()
    {
        $this->secretKey = config('meobox_package_config.teamwork.key');
        $this->url = config('meobox_package_config.teamwork.url');
    }

    public function getPeoples() {

        $action = "companies/96667/people.json";

        $results = $this->getCallTeamworkApi($action);

        return $results->people;
    }

    /**
     * Get All Projects Teamwork with custom data and task lists linked
     *
     * @return array
     */
    public function getProjects() {

        $dbProjects = Projects::all();
        $action = "projects.json";

        $results = $this->getCallTeamworkApi($action);
        $teamwork_projects = $this->sortProjectsByTagName($results->projects, 'PROJETMEO');

        $projects = $this->makeProjectsArray($teamwork_projects, $dbProjects);

        return $projects;
    }

    /**
     * Get Projects Teamwork by search
     *
     * @param $searchTerm (string)
     * @return array
     */
    public function getProjectsBySearchTerm($searchTerm = '') {

        $dbProjects = Projects::all();
        $action = "search.json?searchFor=projects&searchTerm=".urlencode($searchTerm);

        $results = $this->getCallTeamworkApi($action);
        $teamwork_projects = $this->sortProjectsByTagName($results->searchResult->projects, 'PROJETMEO');

        $projects = $this->makeProjectsArray($teamwork_projects, $dbProjects);

        return $projects;
    }

    /**
     * Get All Projects Teamwork with custom data and task lists linked
     *
     * @param $teamwork_projects (array with teamwork projects)
     * @return array
     */
    private function getTaskListByProjectId($project_id) {

        $action = 'projects/'.$project_id.'/tasklists.json';

        $results = $this->getCallTeamworkApi($action);
        $tasklists = $results->tasklists;

        return $tasklists;
    }

    /**
     * Make Final Array Projects
     *
     * @param $teamwork_projects
     * @param $dbProjects
     * @return array
     */
    private function makeProjectsArray($teamwork_projects, $dbProjects){

        $projects = array();
        setlocale(LC_MONETARY, 'fr_CH');

        foreach($teamwork_projects as $key => $project) {

            $budget = 0;
            $suppliers = 0;
            $project_id = $project->id;

            $projects[$project_id]['id'] = $project->id;
            $projects[$project_id]['name'] = $project->name;

            if(isset($project->company)){
                $projects[$project_id]['company'] = $project->company->name;
            }else if(isset($project->companyName)){
                $projects[$project_id]['company'] = $project->companyName;
            }

            $projects[$project_id]['po_client'] = '';
            $projects[$project_id]['project_meo_id'] = '';
            $projects[$project_id]['contact'] = '';
            $projects[$project_id]['user'] = '';
            $projects[$project_id]['budget'] = '';
            $projects[$project_id]['budget_show'] = '';
            $projects[$project_id]['suppliers'] = '';
            $projects[$project_id]['suppliers_show'] = '';

            $projects[$project_id]['time_totals'] = $this->getTimeTotalProject($project->id);
            //$projects[$key]['task_lists'] = $this->getTaskListByProjectId($project->id);

            foreach($dbProjects as $dbProject) {

                if($project->id == $dbProject->id) {

                    $budget = $dbProject->budget;
                    $suppliers = $dbProject->suppliers;
                    $contact = $dbProject->contacts()->get();
                    $user = $dbProject->users()->get();

                    $projects[$project_id]['po_client'] = $dbProject->po_client;
                    $projects[$project_id]['project_meo_id'] = $dbProject->project_meo_id;
                    $projects[$project_id]['contact'] = $contact[0]->first_name.' '.$contact[0]->last_name;
                    $projects[$project_id]['user'] = $user[0]->first_name.' '.$user[0]->last_name;
                    $projects[$project_id]['budget'] = $budget;
                    $projects[$project_id]['budget_show'] = money_format('%i', $budget);
                    $projects[$project_id]['suppliers'] = $suppliers;
                    $projects[$project_id]['suppliers_show'] = money_format('%i', $suppliers);

                }
            }

            $projects[$project_id]['amount_to_date'] = $this->getAmountToDate($projects[$project_id]['time_totals'], $budget, $suppliers);
        }

        return $projects;
    }

    /**
     * Get Total Time Project
     *
     * @param $project_id
     * @return array
     */
    private function getTimeTotalProject($project_id) {

        $time_array = array();
        $action = '/projects/'.$project_id.'/time_entries.json';

        $results = json_decode(json_encode($this->getCallTeamworkApi($action)),true);

        if(isset($results['time-entries']) && !empty($results['time-entries'])) {
            $time_entries = $results['time-entries'];
            $time_array['non_billable_hours_sum'] = 0;
            $time_array['billable_hours_sum'] = 0;
            $time_array['total_hours_sum'] = 0;
            $time_array['users_time_tracking'] = array();

            foreach ($time_entries as $time_entrie) {

                $hours = round($time_entrie['hours'] + ($time_entrie['minutes'] / 60), 2);

                $time_array['users_time_tracking'][$time_entrie['person-first-name'] . '_' . $time_entrie['person-last-name']][] = $hours;

                if ($time_entrie['isbillable']) {
                    $time_array['billable_hours_sum'] += $hours;
                } else {
                    $time_array['non_billable_hours_sum'] += $hours;
                }

                $time_array['total_hours_sum'] += $hours;

            }
        }

        return $time_array;
    }

    /**
     * Make total price and pourcent of budget
     *
     * @param $total_time
     * @param int $budget
     * @return mixed
     */
    private function getAmountToDate($total_time, $budget = 0, $suppliers = 0) {

        $users = Users::all();
        $amount = 0;
        $default_fees = config('meobox_package_config.default_fees');
        $users_time_tracking = (isset($total_time['users_time_tracking']) && !empty($total_time['users_time_tracking'])) ? $total_time['users_time_tracking'] : array();

        if(isset($users_time_tracking) && !empty($users_time_tracking)) {
            foreach($users_time_tracking as $index => $user_time_tracking) {

                $fees = 0;
                $time_total = 0;
                $amount_temp = 0;

                foreach ($users as $user) {
                    if($index == $user->first_name.'_'.$user->last_name) {
                        $fees = $user->fees;
                    }
                }
                foreach($user_time_tracking as $time) {
                    $time_total += $time;
                    if($fees > 0) {
                        $amount += ($time*$fees);
                        $amount_temp += ($time*$fees);
                    }else{
                        $amount += ($time*$default_fees);
                        $amount_temp += ($time*$default_fees);
                    }
                }

                if($fees <= 0) {
                    $full_name = explode('_', $index);
                    $array['warning'][] = $full_name[0].' '.$full_name[1].' pas d\'honoraires défini (par défaut 150 CHF/heure)';
                }

                $array['amount_per_person'][$index]['fees'] = ($fees > 0) ? $fees : $default_fees;
                $array['amount_per_person'][$index]['time'] = $time_total;
                $array['amount_per_person'][$index]['total'] = $amount_temp;

            }
        }

        // Calcul %
        if(!empty($budget)) {
            $pourcent = $budget/100;
            $array['pourcent_to_date'] = round($amount/$pourcent);
        }else{
            $array['pourcent_to_date'] = '0';
        }

        // transforme format CHF
        $array['price_to_date'] = money_format('%i', $amount);

        return $array;
    }

    /**
     * @param array $projects
     * @param string $tagNameToDelete
     * @return array
     */
    private function sortProjectsByTagName($projects = array(), $tagNameToDelete = '') {

        $new_projects_tab = array();

        foreach($projects as $project) {

            $notShowing = false;

            foreach ($project->tags as $tag) {
                if($tag->name == $tagNameToDelete) {
                    $notShowing = true;
                }
            }

            if(!$notShowing) {
                $new_projects_tab[] = $project;
            }
        }

        return $new_projects_tab;
    }

    /**
     * Call the teamwork API to get a multiple data
     *
     * @param $action (string define in https://developer.teamwork.com)
     * @return array
     */
    private function getCallTeamworkApi($action) {

        $channel = curl_init();

        curl_setopt( $channel, CURLOPT_URL, $this->url . $action );
        curl_setopt( $channel, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $channel, CURLOPT_HTTPHEADER,
            array( "Authorization: BASIC ". base64_encode( $this->secretKey .":xxx" ))
        );

        $json = curl_exec ( $channel );

        $results = json_decode($json);

        curl_close ( $channel );

        return $results;
    }
}