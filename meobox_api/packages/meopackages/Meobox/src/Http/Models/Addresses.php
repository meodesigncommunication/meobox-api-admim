<?php

namespace Meopackages\Meobox\Models;

use Illuminate\Database\Eloquent\Model;

class Addresses extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Define a relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts()
    {
        return $this->hasMany('Meopackages\Meobox\Models\Contacts');
    }

    /**
     * Define a relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function countries()
    {
        return $this->belongsTo('Meopackages\Meobox\Models\Countries');
    }
}