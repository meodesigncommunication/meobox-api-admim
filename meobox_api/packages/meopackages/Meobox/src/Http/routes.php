<?php
/**
 * Created by PhpStorm.
 * User: kylemobilia
 * Date: 09.11.17
 * Time: 18:30
 */
/*Route::group(['middleware' => ['web']], function () {

    // Route Administration
    Route::get('/', '\Meopackages\Meobox\Http\Controllers\MeoboxController@index')->middleware('auth');
    Route::get('/dashboard', ['uses' => '\Meopackages\Meobox\Http\Controllers\MeoboxController@index', 'as' => 'dashboard'])->middleware('auth');

    // Authentication routes...
    Route::get('/login', '\Meopackages\Meobox\Http\Controllers\Auth\MeoboxAuthController@index');
    Route::post('/login', ['uses' => '\Meopackages\Meobox\Http\Controllers\Auth\MeoboxAuthController@login', 'as' => 'login']);
    Route::get('/logout', '\Meopackages\Meobox\Http\Controllers\Auth\MeoboxAuthController@logout');

    // Registration routes...
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');

    // Password reset link request routes...
    Route::get('password/email', 'Auth\PasswordController@getEmail');
    Route::post('password/email', 'Auth\PasswordController@postEmail');

    // Password reset routes...
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');

});*/

Route::get('teamwork/projects', '\Meopackages\Meobox\Http\Controllers\MeoboxTeamworkController@getProjects');
Route::get('teamwork/search/{searchTerm}', '\Meopackages\Meobox\Http\Controllers\MeoboxTeamworkController@searchProject');
Route::get('teamwork/peoples', '\Meopackages\Meobox\Http\Controllers\MeoboxTeamworkController@getTeamworkPeoples');
Route::get('users/acc_manager', '\Meopackages\Meobox\Http\Controllers\MeoboxUsersController@getUsersAccManager');

Route::get('contacts', '\Meopackages\Meobox\Http\Controllers\MeoboxContactsController@getContacts');

Route::post('auth/login', '\Meopackages\Meobox\Http\Controllers\MeoboxAuthController@login');
Route::post('auth/logout', '\Meopackages\Meobox\Http\Controllers\MeoboxAuthController@logout');

Route::group(['middleware' => 'jwt'], function () {

    Route::prefix('v1')->group(function () {

        Route::post('register', '\Meopackages\Meobox\Http\Controllers\MeoboxUserController@register');

        Route::post('me', '\Meopackages\Meobox\Http\Controllers\MeoboxAuthController@getAuthUser');

        Route::post('users', '\Meopackages\Meobox\Http\Controllers\MeoboxUsersController@getUsers');
        Route::post('users/acc_manager', '\Meopackages\Meobox\Http\Controllers\MeoboxUsersController@getUsersAccManager');

        Route::post('contacts', '\Meopackages\Meobox\Http\Controllers\MeoboxContactsController@getContacts');

        Route::post('teamwork/peoples', '\Meopackages\Meobox\Http\Controllers\MeoboxTeamworkController@getTeamworkPeoples');

        Route::post('teamwork/projects', '\Meopackages\Meobox\Http\Controllers\MeoboxTeamworkController@getProjects');
        Route::post('teamwork/search', '\Meopackages\Meobox\Http\Controllers\MeoboxTeamworkController@searchProject');
        Route::post('teamwork/project/save', '\Meopackages\Meobox\Http\Controllers\MeoboxTeamworkController@saveProject');

    });

});