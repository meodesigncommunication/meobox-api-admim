<?php namespace Meopackages\Meobox;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class MeoboxServiceProvider extends ServiceProvider{


    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        // Get namespace
        $nameSpace = $this->app->getNamespace();

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        // Routes
        $this->app->router->group(['namespace' => $nameSpace . 'Http\Controllers'], function()
        {
            require __DIR__.'/Http/routes.php';
        });

        // Languages
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'meobox');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/package_config.php', 'meobox_package_config'
        );
    }

}

